package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"time"
	"encoding/json"
	"database/sql"
	_"github.com/go-sql-driver/mysql"
	"os"
)

type Foco struct {
	Id	string    `json:"id"`
	TipoActuador string    `json:"tipo_actuador"`
	Accion	string    `json:"accion"`
	Fecha string    `json:"fecha"`
	IdCasa string    `json:"id_casa"`
	IdUser string    `json:"id_user"`
}

type User struct {
	Id	string    `json:"id_user"`
	IdCasa string    `json:"id_casa"`
	Imei	string    `json:"imei"`
	User	string    `json:"user"`
	Pass	string    `json:"pass"`
}
type Evento struct{
	Id	string    `json:"id"`
	TipoActuador string    `json:"tipo_actuador"`
	Accion	string    `json:"accion"`
	Fecha string    `json:"fecha"`
	IdCasa string    `json:"id_casa"`
	IdUser string    `json:"id_user"`
	NombreUser string	`json:"usuario"`
}

var db	*sql.DB

func main() {
	//db,_ = sql.Open("mysql","domogrupo:domogrupo12345@tcp(electronprog.com:3306)/domo_grupo")
	db,_ = sql.Open("mysql","b8ec694bfb56e0:95919a5d@tcp(us-cdbr-iron-east-05.cleardb.net:3306)/heroku_1e24ffc76a3f6c9")
	r := mux.NewRouter().StrictSlash(false)
	//r.HandleFunc("/actuadores",use(GetFocosHandle,basicAuth)).Methods("GET")
	r.HandleFunc("/actuadores",GetFocosHandle).Methods("GET")
	///r.HandleFunc("/actuadores",use(PostFocosHandle,basicAuth)).Methods("POST")
	r.HandleFunc("/actuadores/",UpdateFocosHandle).Methods("GET")
	r.HandleFunc("/actuadores/all",GetAllFocosHandle).Methods("GET")
	//r.HandleFunc("/actuadores",PostFocosHandle).Methods("POST")
	//r.HandleFunc("/actuadores",use(PutFocosHandle,basicAuth)).Methods("PUT")
	//r.HandleFunc("/actuadores",PutFocosHandle).Methods("PUT")
	r.HandleFunc("/usuarios",GetUserHandle).Methods("GET")
	r.HandleFunc("/user",use(GetUserHandle,basicAuth)).Methods("GET")
	server := &http.Server{
		Addr:	":"+os.Getenv("PORT"),
		Handler:	r,
		ReadHeaderTimeout:	10*time.Second,
		WriteTimeout:	10*time.Second,
		MaxHeaderBytes:	1<<20,
	}
	println("Listening....")
	server.ListenAndServe()
}

func GetUserHandle(w http.ResponseWriter, r *http.Request) {
	var users []User
	var user User
	tab,_ := db.Query("select * from usuario")
	for tab.Next(){
		tab.Scan(&user.Id,&user.IdCasa,&user.Imei,&user.User,&user.Pass)
		users = append(users, user)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(users)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func PutFocosHandle(w http.ResponseWriter, r *http.Request) {
	var focoU Foco
	json.NewDecoder(r.Body).Decode(&focoU)
	env, _ := db.Prepare("UPDATE  actuadores SET accion=?")
	env.Exec(focoU.Accion)
	w.WriteHeader(http.StatusNoContent)
}

func PostFocosHandle(w http.ResponseWriter, r *http.Request) {
	var foco Foco
	json.NewDecoder(r.Body).Decode(&foco)
	env,_ := db.Prepare("INSERT actuadores SET id=?,sensor_luz=?,accion=?,id_casa=?,id_user=?")
	env.Exec(foco.Id,foco.TipoActuador,foco.Accion,foco.IdCasa,foco.IdUser)
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(foco)
	println(string(j))
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}

func GetFocosHandle(w http.ResponseWriter, r *http.Request) {
	var focos []Foco
	var foco Foco
	//tab,_ := db.Query("select * from actuadores")
	tab,_ := db.Query("(SELECT * FROM actuadores  WHERE tipo_actuador='luz1'  ORDER BY id DESC LIMIT 1)UNION(SELECT * FROM actuadores WHERE tipo_actuador='luz2' ORDER BY id DESC LIMIT 1)UNION(SELECT * FROM actuadores  WHERE tipo_actuador='luz3' ORDER BY id DESC LIMIT 1)")
	for tab.Next(){
		tab.Scan(&foco.Id,&foco.TipoActuador,&foco.Accion,&foco.Fecha,&foco.IdCasa,&foco.IdUser)
		focos = append(focos, foco)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(focos)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

func use(h http.HandlerFunc, middleware ...func(http.HandlerFunc) http.HandlerFunc) http.HandlerFunc {
	for _, m := range middleware {
		h = m(h)
	}
	return h
}

func basicAuth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
		var auth bool
		var users []User
		var user User
		tab,_ := db.Query("select * from usuario")
		for tab.Next(){
			tab.Scan(&user.Id,&user.IdCasa,&user.Imei,&user.User,&user.Pass)
			users = append(users, user)
		}
		usr, pass, _ := r.BasicAuth()
		for i:=0; i< len(users);i++{
			 if usr == users[i].User && pass== users[i].Pass{
				auth = true
			 }
		}
		if auth == false {
			http.Error(w, "Not authorized", 401)
			return
		}

		h.ServeHTTP(w, r)
	}
}

func UpdateFocosHandle(w http.ResponseWriter, r *http.Request) {
	tipo_actuador := r.URL.Query().Get("tipo_actuador")
	accion := r.URL.Query().Get("accion")
	usuario := r.URL.Query().Get("usuario")
	println("tipo=",tipo_actuador,"-accion=",accion,"-usuario=",usuario)
	env,_ := db.Prepare("INSERT actuadores SET tipo_actuador=?,accion=?,id_casa=?,id_user=?")
	env.Exec(tipo_actuador,accion,"1",usuario)
	var focos []Foco
	var foco Foco
	tab,_ := db.Query("(SELECT * FROM actuadores  WHERE tipo_actuador='luz1'  ORDER BY id DESC LIMIT 1)UNION(SELECT * FROM actuadores WHERE tipo_actuador='luz2' ORDER BY id DESC LIMIT 1)UNION(SELECT * FROM actuadores  WHERE tipo_actuador='luz3' ORDER BY id DESC LIMIT 1)")
	for tab.Next(){
		tab.Scan(&foco.Id,&foco.TipoActuador,&foco.Accion,&foco.Fecha,&foco.IdCasa,&foco.IdUser)
		focos = append(focos, foco)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(focos)
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}

func GetAllFocosHandle(w http.ResponseWriter, r *http.Request) {
	var eventos []Evento
	var evento Evento
	tab,_ := db.Query("SELECT a.id,a.tipo_actuador,a.accion,a.fecha,a.id_casa,a.id_user,u.user FROM actuadores a INNER JOIN usuario u ON  a.id_user = u.id_user")
	for tab.Next(){
		tab.Scan(&evento.Id,&evento.TipoActuador,&evento.Accion,&evento.Fecha,&evento.IdCasa,&evento.IdUser,&evento.NombreUser)
		eventos = append(eventos, evento)
	}
	w.Header().Set("Content-Type","application/json")
	j,_ := json.Marshal(eventos)
	w.WriteHeader(http.StatusOK)
	w.Write(j)
}